use crate::util::set_channel_nsfw;

// Set the channel to nsfw or removes it
command!(nsfw(_ctx, message) {
    // Get the channel the message was posted in
    if let Some(channel) = message.channel() {
        // Check if it is a guild_channel and retrieve the RwLock
        if let Some(guild_lock) = channel.guild() {
            let guild_channel = guild_lock.read();
            // Send the message 
            if guild_channel.is_nsfw() {
                let _ = guild_channel.send_message(|m| {
                            m.embed(|e| {
                                e.title("NSFW")
                                .description("This channel is now not longer **N**ot **S**afe **F**or **W**ork")
                                .color((255, 0, 0))
                            })
                        });
            } else {
                let _ = guild_channel.send_message(|m| {
                            m.embed(|e| {
                                e.title("NSFW")
                                .description("This channel is now **N**ot **S**afe **F**or **W**ork")
                                .color((255, 0, 0))
                            })
                        });
            }

            // Set the channel nsfw state
            let _ = set_channel_nsfw(guild_channel.id.as_u64().to_owned(), !guild_channel.is_nsfw());
        }
    }
});
