use crate::util::log_message;
use serenity::client::Context;
use serenity::framework::standard::Args;
use serenity::framework::standard::CommandError;
use serenity::model::channel::Message;
use serenity::model::id::ChannelId;
use crate::BOT_CONFIGURATION;

command!(clear(ctx, message, args) {
    let _res = clear_channel(ctx, message, args, |_m|true);
});

command!(uclear(ctx, message, args) {
    // TODO get the mentions and filter the messages based on the author
    let _res = clear_channel(ctx, message, args, |m| {
        if message.mentions.contains(&m.author) {
            return true;
        }
        false
    });
});

command!(sclear(ctx, message, args) {
    // Clone the args so that we can still pass them on
    if let Ok(keywords) = args.clone().multiple::<String>() {
        // Clear the channel
        let _res = clear_channel(ctx, message, args, |m| {
            // Filter based on the keywords
            for keyword in &keywords {
                println!("{}, {}", keyword, m.content);
                if m.content.contains(keyword) {
                    return true;
                }
            } 
            false
        });
    }
});

fn clear_channel<F>(
    ctx: &mut Context,
    message: &Message,
    _args: Args,
    f: F,
) -> Result<(), CommandError>
where
    F: FnMut(&Message) -> bool,
{
    // Lock the context so that we can use it and retrieve the channel
    if let (Some(channel), Some(data)) = (message.channel(), ctx.data.try_lock()) {
        // Save the log_channel in a variable for later usage
        let mut log_channel_id = None;
        message.guild_id.map(|guild_id| {
            BOT_CONFIGURATION.get_guild(guild_id.0).map(|lock| lock.try_read().map(|guard| log_channel_id = guard.log_channel_id));
        });
        // We are only able to delete multiple messages in a GuildChannel
        if let Some(guild_channel_lock) = channel.guild() {
            // Read the GuildChannel and save it to a variable
            let guild_channel = guild_channel_lock.read();
            // Get the messages to delete in the channel
            let mut messages = guild_channel.messages(|c| c)?;
            messages.retain(f);
            // Retrieve and the log_channel_id if possible
            if let Some(log_channel_id) = log_channel_id {
                let mut data_deleted_messages: Vec<u8> = Vec::new();
                // Go backwards through the list so that the messages appear in the right order
                for i in (0..messages.len()).rev() {
                    let message = &messages[i];
                    // Append the message id
                    data_deleted_messages
                        .append(&mut message.id.clone().as_u64().to_string().into_bytes());
                    data_deleted_messages.append(&mut " (".to_owned().into_bytes());
                    // Append the Author name
                    data_deleted_messages.append(&mut message.author.name.clone().into_bytes());
                    data_deleted_messages.append(&mut ", ".to_owned().into_bytes());

                    // Append the author id
                    data_deleted_messages
                        .append(&mut message.author.id.as_u64().to_string().into_bytes());

                    data_deleted_messages.append(&mut ") ".to_owned().into_bytes());
                    // Append the message content
                    data_deleted_messages.append(&mut message.content.clone().into_bytes());
                    data_deleted_messages.append(&mut " \n".to_owned().into_bytes());
                }

                // Convert the Vec to a slice so we can use it in the send_files function
                let byte_data_deleted_messages: &[u8] = &data_deleted_messages;
                let log_channel = ChannelId(log_channel_id);
                // Log the message to the log channel
                log_message(log_channel, |m| {
                    m.embed(|e| {
                        e.title("Info")
                            .field("Channel", guild_channel.id, true)
                            .field("Message Amount", messages.len() as u32, true)
                            .description("Channel has been cleared")
                            .color((217, 217, 67))
                    })
                });
                // Send the file with the deleted messages to the channel
                // This was separated to show the upload under the information message
                let _res = log_channel.send_files(
                    vec![(byte_data_deleted_messages, "deleted_messages.txt")],
                    |m| m,
                );
            }

            let _ = guild_channel.delete_messages(messages);
            let _res = guild_channel.send_message(|m| {
                m.embed(|e| {
                    e.title("Information")
                        .description("This channel has been cleared")
                        .color((217, 217, 67))
                })
            });
            return Ok(());
        }
    }

    Err(CommandError(String::from(
        "The clear_channel function failed to execute",
    )))
}
