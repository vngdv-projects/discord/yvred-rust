use crate::self_roles::SelfRoles;
use serde_json::map::Map;
use serde_json::Value;
use serenity::builder::CreateMessage;
use serenity::client::Context;
use serenity::framework::standard::{Args, CommandOptions};
use serenity::http::raw::edit_channel;
use serenity::model::channel::{GuildChannel, Message, Reaction, ReactionType};
use serenity::model::id::{ChannelId, RoleId};
use serenity::model::permissions::Permissions;

pub fn log_message<F>(channel_id: ChannelId, f: F) -> ()
where
    F: FnOnce(CreateMessage) -> CreateMessage,
{
    let _result = channel_id.send_message(f);
}

pub fn get_permissions(
    _: &mut Context,
    msg: &Message,
    _: &mut Args,
    _: &CommandOptions,
) -> Permissions {
    if let Some(member) = msg.member() {
        if let Ok(permissions) = member.permissions() {
            return permissions;
        }
    }

    Permissions::empty()
}

pub fn set_channel_nsfw(channel_id: u64, nsfw_state: bool) -> serenity::Result<GuildChannel> {
    let mut new_map = Map::new();
    new_map.insert("nsfw".to_owned(), Value::Bool(nsfw_state));
    edit_channel(channel_id, &new_map)
}

// Todo maby fix this method it is not as nice as the other functions
pub fn assign_role_from_reaction(ctx: Context, reaction: Reaction, remove: bool) {
    // Read the config
    if let Some(data) = ctx.data.try_lock() {
        let self_roles = data.get::<SelfRoles>().unwrap();
        // Read the messages_ids to check if this is the right message
        if self_roles
            .message_ids
            .contains(reaction.message_id.as_u64())
        {
            // Get the message from the reaction
            if let Ok(channel) = reaction.channel() {
                // Retrive the guild from the message
                if let Some(guild_channel_locked) = channel.guild() {
                    // Read the guild_channel_locked so that we can use the GuildChannel
                    let guild_channel = guild_channel_locked.read();
                    // Get the Guild
                    if let Some(guild_locked) = guild_channel.guild() {
                        // Read the guild lock so that we can search for the member
                        let guild = guild_locked.read();
                        // Search for the member
                        if let Ok(mut member) = guild.member(reaction.user_id) {
                            // Retrive the Reaction
                            if let ReactionType::Custom {
                                animated: _,
                                id,
                                name: _,
                            } = reaction.emoji
                            {
                                if let Some(role_id) = self_roles.emoji_roles.get(id.as_u64()) {
                                    // Convert the u64 to a actual RoleId
                                    let role = RoleId(role_id.to_owned());
                                    // Add or remove the role of the member
                                    if remove {
                                        let _res = member.remove_role(role);
                                    } else {
                                        let _res = member.add_role(role);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
