use crate::configuration::{BotConfiguration, Guild};
use crate::util::assign_role_from_reaction;
use crate::util::log_message;
use crate::BOT_CONFIGURATION;
use serenity::client::Context;
use serenity::model::channel::Reaction;
use serenity::model::gateway::{Game, Ready};
use serenity::model::guild::Member;
use serenity::model::id::{ChannelId, GuildId};
use serenity::model::user::OnlineStatus;
use serenity::prelude::EventHandler as SerenityEventHandler;

// Create the EventHandler struct to later implement the SerenityEventHandler
pub struct EventHandler;

// Implement the SerenityEventHandler type for the EventHandler
impl SerenityEventHandler for EventHandler {
    fn guild_member_addition(&self, ctx: Context, guild_id: GuildId, new_member: Member) {
        let mut log_channel_id: Option<u64> = None;
        // Read the config and search for the log_channel_id
        BOT_CONFIGURATION.get_guild(guild_id.0).map(|guild_lock| {
            guild_lock.try_read().map(|guild_guard| {
                log_channel_id = guild_guard.log_channel_id;
            });
        });

        // Check if this guild has an log_channel
        match log_channel_id {
            Some(id) => {
                // Create ChannelId object from the channel id u64
                let log_channel = ChannelId(log_channel_id.unwrap());
                // Send a message to the logging channel
                log_message(log_channel, |m| {
                    m.embed(|e| {
                        e.title("Member joined the guild!")
                            .description("It seems like this guild is growing!")
                            .field("User", new_member.distinct(), true)
                            .field("ID", new_member.user_id().as_u64(), true)
                            .color((255, 0, 0))
                    })
                })
            }
            None => {}
        }
    }

    fn reaction_add(&self, ctx: Context, add_reaction: Reaction) {
        assign_role_from_reaction(ctx, add_reaction, false);
    }

    fn reaction_remove(&self, ctx: Context, removed_reaction: Reaction) {
        assign_role_from_reaction(ctx, removed_reaction, true);
    }

    fn ready(&self, ctx: Context, ready: Ready) {
        info!("Started. Logged in as: {}", ready.user.name);
        // Load the presence from the config
        match &BOT_CONFIGURATION.presence {
            Some(p) => {
                ctx.set_presence(Some(Game::playing(p)), OnlineStatus::Online);
            }
            None => {
                ctx.set_presence(None, OnlineStatus::Online);
            }
        }
    }
}
