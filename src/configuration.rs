use crate::{ConfigurationError, Result};
use std::env;
use std::fs::File;
use std::io::Read;
use std::sync::RwLock;
use typemap::Key;

#[derive(Deserialize, Serialize)]
pub struct BotConfiguration {
    guilds: Vec<RwLock<Guild>>,
    /// The global command prefix for this bot.
    pub command_prefix: String,
    /// Holds the Token for the bot to authenticate on Discord.
    pub token: String,
    /// This holds the status that will be applied to the bot on start.
    /// It will show as something like "Playing: A nice game.
    pub presence: Option<String>,
}

impl BotConfiguration {
    /// Create the BotConfiguration struct from the contents of a specified json file.
    ///
    /// # Examples
    ///
    /// ```
    /// let bot_config = BotConfiguration::new_from_file("config.json");
    /// ```
    pub fn new_from_file(file: &str) -> BotConfiguration {
        // Create a file object that points to the file that holds the configuration
        // and read everything that's in there
        let mut config_contents = String::new();
        // Opens the config file
        let mut file = File::open(file)
            // File does not exist
            // TODO Create a new base file
            .expect("The config file does not exist!");
        file.read_to_string(&mut config_contents)
            .expect("Unable to read the configuration file!");
        // Use serde_json to load the config file
        let config: BotConfiguration = serde_json::from_str(&config_contents)
            // If this happens the Json file is probably wrong.
            .expect("Corrupted config file!");
        config
    }

    /// This method returns a RwLock to the requested guild configuration on success.
    ///
    /// # Examples
    ///
    /// ```
    /// // This holds the configuration to a guild.
    /// let guild: RwLock<Guild> = bot_config.get_guild(526179142740344832u64).unwrwap();
    /// ```
    pub fn get_guild(&self, guild_id: u64) -> Result<&RwLock<Guild>> {
        // Creates the return variable
        let mut result: Result<&RwLock<Guild>> = Err(ConfigurationError::GuildNotFound.into());
        // Looks through the guilds to find the right one
        for guild in &self.guilds {
            guild
                .try_read()
                .map(|guild_guard| {
                    // This compares the guild_id of the locked guild with the supplied guild_id
                    if guild_guard.guild_id == guild_id {
                        result = Ok(&guild);
                    }
                })
                .map_err(|t| {});
        }
        return result;
    }

    /// Returns the bot token that is already saved.
    /// If a there is an environment variable with the name DISCORD_TOKEN it will be prioritized.
    pub fn get_token(&self) -> Result<String> {
        match env::var("DISCORD_TOKEN").and_then(|token| {
            return Ok(token);
        }) {
            Err(e) => Ok(self.token.clone()),
            Ok(t) => Ok(t),
        }
    }
}

// Represents a Guild Configuration
#[derive(Deserialize, Serialize)]
pub struct Guild {
    pub guild_id: u64,
    pub log_channel_id: Option<u64>,
}
