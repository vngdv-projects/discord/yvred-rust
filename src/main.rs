#[macro_use]
extern crate serenity;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate typemap;
#[macro_use]
extern crate lazy_static;

// Modules are added here
// The handler module is used to hold the EventHandler struct
mod commands;
mod configuration;
mod handler;
mod result;
mod self_roles;
mod util;

use serenity::client::Client;
use serenity::framework::standard::StandardFramework;
use std::env;

use crate::handler::EventHandler;
use crate::self_roles::SelfRoles;
use crate::util::get_permissions;

//Reexports
pub use configuration::BotConfiguration;
pub use result::{ConfigurationError, Error, Result};

use crate::commands::{
    clear_command, info_command, nsfw_command, roles_add, roles_list, roles_remove, sclear_command,
    shutdown_command, uclear_command,
};

lazy_static! {
    static ref BOT_CONFIGURATION: BotConfiguration = BotConfiguration::new_from_file("config.json");
}

fn main() {
    // Initiate the logging crate
    env_logger::init();
    info!("Start initiated");

    // Login with a bot token from the environment
    let mut client = Client::new(
        &BOT_CONFIGURATION
            .get_token()
            .expect("Discord token could not be found!"),
        EventHandler,
    )
    .expect("Error creating client");

    client.with_framework(
        StandardFramework::new()
            .configure(|c| c.allow_dm(false).prefix(&BOT_CONFIGURATION.command_prefix)) // set the bot's prefix to "~"
            .cmd("info", info_command)
            .command("clear", |c| {
                // Only execute for users with manage_messages permission
                c.check(|ctx, message, args, options| {
                    get_permissions(ctx, message, args, options).manage_messages()
                })
                .cmd(clear_command)
            })
            .command("uclear", |c| {
                // Only execute for users with manage_messages permission
                c.check(|ctx, message, args, options| {
                    get_permissions(ctx, message, args, options).manage_messages()
                })
                .cmd(uclear_command)
            })
            .command("sclear", |c| {
                // Only execute for users with manage_messages permission
                c.check(|ctx, message, args, options| {
                    get_permissions(ctx, message, args, options).manage_messages()
                })
                .cmd(sclear_command)
            })
            .command("nsfw", |c| {
                // Only execute if the user has manage_channel permission
                c.check(|ctx, message, args, options| {
                    get_permissions(ctx, message, args, options).manage_channels()
                })
                .cmd(nsfw_command)
            })
            .command("shutdown", |c| {
                // Only execute for users with Administration rights
                // TODO only let bot admins execute this command
                c.check(|ctx, message, args, options| {
                    get_permissions(ctx, message, args, options).administrator()
                })
                .cmd(shutdown_command)
            })
            .group("roles", |g| {
                g.command("add", |c| c.cmd(roles_add))
                    .command("remove", |c| c.cmd(roles_remove))
                    .command("list", |c| c.cmd(roles_list))
            }),
    );

    {
        // Get the client context data and lock it so we can edit it.
        let mut data = client.data.lock();
        data.insert::<SelfRoles>(SelfRoles::new());
    }

    // start listening for events by starting a single shard
    if let Err(why) = client.start() {
        println!("An error occurred while running the client: {:?}", why);
    }
}
