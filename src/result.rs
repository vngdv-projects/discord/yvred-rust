use crate::result::Error::Configuration;
use std;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Configuration(ConfigurationError),
    NoneError,
}

impl From<ConfigurationError> for Error {
    fn from(e: ConfigurationError) -> Self {
        Configuration(e)
    }
}

#[derive(Debug)]
pub enum ConfigurationError {
    GuildNotFound,
    NoTokenFound,
}
